# Node Task

## Nanuq the Alaskan Malamute
![Nanuq](nanuq.jpg "Nanuq")


```
 “Do the best you can until you know better. Then when you know better, do better”
 —Maya Angelou
 ```
 

 ## Python-koodia
 
```python
eka = int(input("syötä eka numero "))
toka = int(input("syötä toka numero "))

summa = eka+toka

print("Numeroiden summa on: " ,(summa))
```